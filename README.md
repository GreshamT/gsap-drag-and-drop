# Drag and drop + click to add example with GSAP

This internal project is to build drag and drop AND click to add functionality using GSAP.

Replica of this example https://gitlab.com/GreshamT/phaser-drag-and-drop created in Phaser 3.

# Hosted link

https://warm-heliotrope-6751eb.netlify.app/