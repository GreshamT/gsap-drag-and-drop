import { gsap } from "gsap";
import "./style.css";
import "./game.js";
import { Draggable } from "gsap/Draggable";

gsap.registerPlugin(Draggable);

// Variable to store the selected image
let selectedImg = null;

window.handleItems = function (data) {
    var imageContainer = document.getElementById("imageContainer");

    // Loop through each item in the data
    data.forEach(function (item) {
        // Construct the image URL
        var imageUrl =
            "https://s.d3sv.net/ip/100x,jpg/" + encodeURIComponent(item.image);

        // Create an img element
        var img = document.createElement("img");
        img.src = imageUrl;

        // Append the img element to the imageContainer
        imageContainer.appendChild(img);
        img.dataset.name = item.name;
        // Add click event listener to the image
        img.addEventListener("click", function () {
            // Remove the selected class from the previous selected image
            if (selectedImg) {
                selectedImg.style.backgroundColor = "";
            }

            // Set the new selected image and add the selected class to it
            selectedImg = img;
            img.style.backgroundColor = "lightcoral";
        });

        dragItems(img);
    });
};

var script = document.createElement("script");
script.src = "https://p.d3sv.net/311361825/v2/r?k=kw17&callback=handleItems";

document.head.appendChild(script);

function dragItems(img) {
    let originalX = gsap.getProperty(img, "x");
    let originalY = gsap.getProperty(img, "y");
    let isOverCart = false; // Variable to store whether the image is over the cart

    Draggable.create(img, {
        type: "x,y",
        bounds: ".mainContainer",
        onDrag: function () {
            let cart = document.getElementById("shoppingCart");
            isOverCart = Draggable.hitTest(img, cart, "50%"); // Check if the image is over the cart while dragging
        },
        onDragEnd: function () {
            if (isOverCart) {
                snapToCart(this.target); // If the image is over the cart, snap it to the cart
            } else {
                snapArea(this.target, originalX, originalY); // If the image is not over the cart, return it to its original position
            }
        },
    });
}

// Function to determine intersection
function snapArea(target, originalX, originalY) {
    gsap.to(target, {
        x: originalX,
        y: originalY,
        scale: 1.0, // Restore the original size
        duration: 0.5,
    });
}

// Add click event listener to the "Add to cart" button
document.getElementById("addToCart").addEventListener("click", function () {
    if (selectedImg) {
        snapToCart(selectedImg);
        selectedImg = null;
    }
});

// Function to snap the image to the cart
function snapToCart(target) {
    let cart = document.getElementById("shoppingCart");
    let cartRect = cart.getBoundingClientRect();
    let imgRect = target.getBoundingClientRect();

    let cartCenterX = cartRect.left + cartRect.width / 2 - imgRect.width / 4; // Adjusting for the size of the image and the cart
    let cartCenterY = cartRect.top + cartRect.height / 2 - imgRect.height / 4; // Adjusting for the size of the image and the cart

    gsap.to(target, {
        xPercent: ((cartCenterX - imgRect.left) / imgRect.width) * 100, // Subtract the initial position of the image to get the relative movement required
        yPercent: ((cartCenterY - imgRect.top) / imgRect.height) * 100, // Subtract the initial position of the image to get the relative movement required
        scale: 0.5, // Reduce the size to half
        duration: 0.5,
        onComplete: function () {
            target.style.backgroundColor = ""; // Remove the background color
            document.getElementById("cartItems").innerHTML += `
            <ul>
                <li>${target.dataset.name}</li>
            </ul>
            `; // Add the item's name to the cartItems div
        },
    });
    // console.log("snapped");
}
